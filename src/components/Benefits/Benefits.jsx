import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faShapes } from '@fortawesome/free-solid-svg-icons';

import Benefit from '../Benefit/Benefit';
import benefit_image from '../../img/benefit_image.jpg';

import './Benefits.scss';

function Benefits() {
    return(
        <div className="Benefits">
            <img className="Benefits__content__image" src={benefit_image} alt="Mobile" />
            <div className="Benefits__content">
                <h3 className="Benefits__title">¿Por qué elegir Cabify?</h3>
                <ul className="Benefits__content__list">
                    <li class="Benefits__list__item">
                        <Benefit
                            icon={faUser}
                            title="Tu seguridad es nuestra prioridad"
                            description="Con viajes geolocalizados y conductores identificados. Además, ponemos medidas de protección contra el COVID-19 para que viajas sin preocupaciones."
                        />
                    </li>
                    <li class="Benefits__list__item">
                        <Benefit
                            icon={faCheck}
                            title="El estándar de calidad más alto"
                            description="Conductores profesionales, vehículos modernos y posibilidad de elegir tus preferencias de viaje de la forma más fácil. Por fin viajarás como te mereces."
                        />
                    </li>
                    <li class="Benefits__list__item">
                        <Benefit 
                            icon={faShapes}
                            title="Claridad y transparencia en los precios"
                            description="Siempre te mostramos el precio antes de aceptar el viaje para que tú decidas cómo moverte por la ciudad. Sin sorpresas."
                        />
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default Benefits;