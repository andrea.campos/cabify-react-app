import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './Benefit.scss';

function Benefit(props) {
    return(
        <section className="Benefit">
            <FontAwesomeIcon className="Benefit__icon" icon={props.icon} alt="User" />
            <div className="Benefit__container">
                <h4 className="Benefit__container__title">{props.title}</h4>
                <p className="Benefit__container__description">{props.description}</p>
            </div>
        </section>
    )
}

export default Benefit;