import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLongArrowAltRight } from '@fortawesome/free-solid-svg-icons'

import header_img from '../../img/header_img.jpg';

import './Header.scss';

function Header() {
    return(
        <div className="Header">
            <div className="Header__container">
                <h1 class="Header__title">Más seguridad, mayor calidad y una ciudad a tu alcance</h1>
                <p class="Header__subtitle">Conviértete en conductor y empieza a ganar dinero</p>
                <button className="Header__button">Conduce con nosotros</button>
                <div className="Header__link">
                    <a href="#" className="Header__link__text">¿Eres un gestor de flota? <FontAwesomeIcon icon={faLongArrowAltRight}/></a>
                </div>
            </div>
            <img class="Header__image" src={header_img} alt="Header" />
        </div>
    )
}

export default Header;