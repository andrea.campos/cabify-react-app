import Div100vh from 'react-div-100vh'

import Header from './components/Header/Header';
import Benefits from './components/Benefits/Benefits';

import "./App.scss";

function App() {
  return (
    <Div100vh className="App">
      <Header/>
      <Benefits/>
    </Div100vh>
  );
}

export default App;
